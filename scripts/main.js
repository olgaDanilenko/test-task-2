var form = new Vue({
    el: '#form',
    data: {
        fio: '',
        errors: '',
        fioObj: {
            firstName: '',
            lastName: '',
            patronymic: ''
        }
    },
    methods: {
        validateFIO: function() {
            var pattern1 = /(^[a-zа-я]?[a-zа-я- ]+){1,3}$/ig;
            var array = [];
            if(this.fio.search(pattern1) != 0) {

                this.errors = 'Неправильно введены данные';
                this.fioObj.firstName = '';
                this.fioObj.lastName = '';
                this.fioObj.patronymic = '';
            } else {
                this.errors = '';
                array = this.fio.split(/[\s]+/g);
                this.fioObj.firstName = array.length >= 1 ? array[0] : '';
                this.fioObj.lastName = array.length >= 2 ? array[1] : '';
                this.fioObj.patronymic = array.length == 3 ? array[2] : '';
                if(array.length > 3){
                    this.fio = array.slice(0, 3).join(' ');
                }
            }

        }
    }
});

Vue.component('modal', {
    template: '#modal-template',
    data: function() {
        return form;
    }
});

new Vue({
    el: '#app',
    data: {
        showModal: false
    }
});